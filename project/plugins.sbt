/** https://github.com/codacy/sbt-codacy-coverage#sbt-codacy-coverage */
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

/** https://maven-badges.herokuapp.com/maven-central/com.codacy/sbt-codacy-coverage */
addSbtPlugin("com.codacy" % "sbt-codacy-coverage" % "2.112")
